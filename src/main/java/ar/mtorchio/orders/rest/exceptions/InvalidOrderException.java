package ar.mtorchio.orders.rest.exceptions;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.util.Objects;

@NoArgsConstructor
@AllArgsConstructor
public class InvalidOrderException extends RuntimeException {

    @NonNull
    private String field;

    private Boolean isRequired = false;

    private String customMessage;

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder(
                "OrderNotFoundException {field="+field+", " +
                "isRequired= "+isRequired+", " );
        if( Objects.nonNull(customMessage) ){
            sb.append(", message= "+customMessage);
        }
        return sb.append("} ").toString();
   }
}
