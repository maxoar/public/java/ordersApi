package ar.mtorchio.orders.rest.exceptions;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@NoArgsConstructor
@AllArgsConstructor
public class OrderNotFoundException extends RuntimeException {

    @NonNull
    private Integer id;

    @Override
    public String toString(){
        return "OrderNotFoundException [id="+id+"] ";
    }

}

