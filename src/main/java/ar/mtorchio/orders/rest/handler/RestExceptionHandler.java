package ar.mtorchio.orders.rest.handler;

import ar.mtorchio.orders.exception.RestException;
import ar.mtorchio.orders.rest.exceptions.InvalidOrderException;
import ar.mtorchio.orders.rest.exceptions.OrderNotFoundException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler{// extends ResponseEntityExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<RestException> handleException(Throwable ex) {
        String error = "Internal Server Error";
        return buildResponseEntity( new RestException( HttpStatus.INTERNAL_SERVER_ERROR, error, ex.getLocalizedMessage() ) );
    }

    @ExceptionHandler(InvalidOrderException.class)
    public ResponseEntity<RestException> exceptionInvalidOrderException(InvalidOrderException ex) {
        String error = "Malformed JSON request";
        return buildResponseEntity(new RestException(HttpStatus.BAD_REQUEST, error, ex.toString()));
    }

    @ExceptionHandler(OrderNotFoundException.class)
    public ResponseEntity<RestException> exceptionOrderNotFoundException(OrderNotFoundException ex) {
        String error = "Order not found";
        return buildResponseEntity(new RestException(HttpStatus.NOT_FOUND, error, ex.toString()));
    }

    public ResponseEntity<RestException> buildResponseEntity(RestException apiError) {
        return new ResponseEntity<>(apiError, apiError.getHttpStatus());
    }
}
