package ar.mtorchio.orders.rest.endpoint;

import ar.mtorchio.orders.bo.OrderBo;
import ar.mtorchio.orders.model.Order;
import ar.mtorchio.orders.dto.OrderDTO;
import ar.mtorchio.orders.rest.exceptions.OrderNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "https://secret-ridge-15376.herokuapp.com")
public class OrderEndpoint {

    @Autowired
    OrderBo orderBo;

    @GetMapping("/orders/{id}")
    public ResponseEntity<OrderDTO> getById(@PathVariable("id") Integer id) {
        if( Objects.isNull(id) ) throw new IllegalArgumentException("The received parameter is invalid or not supplied");
        Order order = orderBo.getById(id);
        if( Objects.isNull(order) ) throw new OrderNotFoundException(id);
        return ResponseEntity.ok().body(new OrderDTO(order));
    }

    @PostMapping(value = "/orders/save", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<OrderDTO> newOrder(@RequestBody OrderDTO orderDTO){
        orderDTO.isValidOrder();
        Order order = orderDTO.toOrder();
        orderBo.saveOrUpdate(order);
        return ResponseEntity.status(HttpStatus.CREATED).body(new OrderDTO(order));
    }

    @PutMapping(value="/orders/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<OrderDTO> updateOrder(@RequestBody OrderDTO orderDTO, @PathVariable("id") Integer id){
        Order order = orderDTO.toOrder();
        orderBo.saveOrUpdate(order);
        return ResponseEntity.ok().body(new OrderDTO(order));
    }

    @DeleteMapping("/orders/{id}")
    public ResponseEntity<String> deleteOrder(@PathVariable("id") Integer id){
        Order order = orderBo.getById(id);
        orderBo.delete(order);
        return ResponseEntity.ok().body(id + " order deleted.");
    }
}
