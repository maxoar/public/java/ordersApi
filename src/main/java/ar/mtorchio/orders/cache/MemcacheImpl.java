package ar.mtorchio.orders.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class MemcacheImpl implements Memcache {

    private static Map<String, Object> memcache;// = new ConcurrentHashMap<>();

    private static Logger logger = LoggerFactory.getLogger(MemcacheImpl.class);

    public MemcacheImpl(){
        memcache = new ConcurrentHashMap<>();
    }

    @Override
    public void set(String key, Object value){
        logger.info("Se accede a la cache :: set()");
        memcache.put(key, value);
    }

    @Override
    public Object get(String key){
        logger.info("Se accede a la cache :: get()");
        return memcache.get(key);
    }

    @Override
    public void delete(String key){
        logger.info("Se accede a la cache :: delete()");
        memcache.remove(key);
    }



}
