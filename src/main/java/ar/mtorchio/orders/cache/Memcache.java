package ar.mtorchio.orders.cache;

public interface Memcache {

    void set(String key, Object value);

    Object get(String key);

    void delete(String key);

}
