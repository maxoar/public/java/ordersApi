package ar.mtorchio.orders.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * Created by mtorchio on 01/03/2018.
 */
@Configuration
@EnableJpaRepositories(basePackages = "ar.mtorchio.orders.dao")
@PropertySource("classpath:persistence-sqlite.properties")
public class SQLiteConfig {

    private static final String DOMAIN_BASE_PACKAGE = "ar.mtorchio.orders.model";

    @Value("${spring.datasource.url}")
    private String url;

    @Value("${spring.datasource.driverClassName}")
    private String driverClassName;

    @Value("${spring.datasource.username}")
    private String username;

    @Value("${spring.datasource.password}")
    private String password;

    @Value("${hibernate.dialect}")
    private String hibernateDialect;

    @Value("${hibernate.show_sql}")
    private String hibernateShowSql;

    @Value("${hibernate.hbm2ddl.auto}")
    private String hibernateHbm2ddAuto;

    /*@Autowired
    Environment env;*/

    @Bean
    public DataSource dataSource() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName( driverClassName );
        dataSource.setUrl( url );
        dataSource.setUsername( username );
        dataSource.setPassword( password );
        return dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        final LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setPersistenceUnitName("orders-unit");
        em.setDataSource(dataSource());
        em.setPackagesToScan(new String[] { DOMAIN_BASE_PACKAGE });
        em.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        em.setJpaProperties(additionalProperties());
        return em;
    }

    final Properties additionalProperties() {
        final Properties hibernateProperties = new Properties();
        if (hibernateHbm2ddAuto != null) {
            hibernateProperties.setProperty("hibernate.hbm2ddl.auto", hibernateHbm2ddAuto);
        }
        if (hibernateDialect != null) {
            hibernateProperties.setProperty("hibernate.dialect", hibernateDialect );
        }
        if (hibernateShowSql != null) {
            hibernateProperties.setProperty("hibernate.show_sql", hibernateShowSql);
        }
        return hibernateProperties;
    }
}


