package ar.mtorchio.orders.config;

import ar.mtorchio.orders.cache.Memcache;
import ar.mtorchio.orders.cache.MemcacheImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class CacheConfig {

    @Bean
    @Scope(value = "singleton")
    public Memcache BumexMemcached(){
        return new MemcacheImpl();
    }
}
