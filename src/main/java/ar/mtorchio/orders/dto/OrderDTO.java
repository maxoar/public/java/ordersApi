package ar.mtorchio.orders.dto;

import ar.mtorchio.orders.model.Order;
import ar.mtorchio.orders.rest.exceptions.InvalidOrderException;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.Objects;

@Data
@AllArgsConstructor
public class OrderDTO {

    private String idOrder;

    private String name;

    private String price;

    private String discount;

    public OrderDTO(@NotNull Order source) {
        this(source.getIdOrder() != null ? source.getIdOrder().toString() : null,
                source.getName(),
                source.getPrice() != null ? source.getPrice().toString() : null,
                source.getDiscount() != null ? source.getDiscount().toString() : null
        );
    }

    public Order toOrder() {
        return new Order(
                this.idOrder != null ? Integer.valueOf(this.idOrder) : null,
                this.name,
                this.price != null ? Integer.valueOf(this.price) : null,
                this.discount != null ? Integer.valueOf(this.discount) : null
        );
    }

    public void isValidOrder() {
        if( Objects.isNull(name) ){
            throw new InvalidOrderException(name, true, "Order name, must be supplied.");
        } else if (name.isEmpty()) {
            throw new InvalidOrderException(name, true, "Name can't be empty.");
        }

        if( Objects.isNull(price) ){
            throw new InvalidOrderException(price, true, "Order price, must be supplied.");
        } else if ( ! price.matches("^\\d+") ) { //check if number;
            throw new InvalidOrderException(price, true, "Price must be valid number.");
        }

        if( Objects.isNull(price) ) {
            if (!discount.matches("/^\\d+$/;")) {
                throw new InvalidOrderException(discount, false, "Discount must be valid number.");
            } else if (Integer.valueOf(discount) > 100) {
                throw new InvalidOrderException(discount, false, "Discount can't be greater than 100%.");
            }
        }
    }
}