package ar.mtorchio.orders.dao;

import ar.mtorchio.orders.model.Order;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderDAO {

    void insertOrUpdate(Order order);

    void delete(Order order);

    Order select(Integer idOrder);

}
