package ar.mtorchio.orders.dao.impl;

import ar.mtorchio.orders.dao.OrderDAO;
import ar.mtorchio.orders.model.Order;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.transaction.Transactional;
import java.util.Objects;

@Repository
public class OrderDAOImpl implements OrderDAO {

    @PersistenceContext(unitName = "orders-unit", type = PersistenceContextType.TRANSACTION)
    private EntityManager em;

    @Override
    @Transactional
    public void insertOrUpdate(Order order) {
        Objects.requireNonNull(order, "Order can not be null.");
        if( order.getIdOrder() != null ){
            em.merge(order);
        } else {
            em.persist(order);
        }
        em.flush();
    }

    public void delete(Order order){
        em.remove(order);
    }

    public Order select(Integer idOrder){
        return em.find(Order.class, idOrder);
    }
}
