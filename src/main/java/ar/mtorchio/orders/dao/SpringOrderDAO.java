package ar.mtorchio.orders.dao;

import ar.mtorchio.orders.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface SpringOrderDAO extends JpaRepository<Order, Integer> {

}
