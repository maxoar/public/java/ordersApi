package ar.mtorchio.orders.bo;

import ar.mtorchio.orders.cache.Memcache;
import ar.mtorchio.orders.dao.OrderDAO;
import ar.mtorchio.orders.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderBo {

    @Autowired
    OrderDAO orderDAOImpl;

    //@Autowired
    //SpringOrderDAO springOrderDAO;

    @Autowired
    Memcache memcache;

    public Order getById(Integer id ) {
        Order order = (Order) memcache.get( id.toString() );
        if( order == null ){
            //order = springOrderDAO.findById(id).orElseThrow( () -> new OrderNotFoundException(id) );
            order = orderDAOImpl.select( id );
            if( order == null ) { return null; }//throw new OrderNotFoundException(id);
            memcache.set(order.getIdOrder().toString(), order);
        }
        return order;
    }

    public Order saveOrUpdate(Order order){
        //springOrderDAO.save(order);
        orderDAOImpl.insertOrUpdate( order );
        memcache.set( order.getIdOrder().toString(), order );
        return order;
    }

    public void delete(Order order){
        Integer idOrder = order.getIdOrder();
        orderDAOImpl.delete( order );
        memcache.delete( idOrder.toString() );
    }
}
