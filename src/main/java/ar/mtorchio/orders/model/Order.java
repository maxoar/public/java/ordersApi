package ar.mtorchio.orders.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor //required by hibernate
@AllArgsConstructor
@Entity
@Table(name = "orders")
public class Order implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="idOrder")
    private Integer idOrder;

    @NonNull
    @Column(name="name")
    private String name;

    @NonNull
    @Column(name="price")
    private Integer price;

    @Column(name="discount")
    private Integer discount;
}

