package ar.mtorchio.orders;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrdersApplication {

	//private static final Logger logger = LogManager.getLogger(OrdersApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(OrdersApplication.class, args);
	}

}



