package ar.mtorchio.orders.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class RestException {

    private HttpStatus httpStatus;
    private String message;
    private String debugMessage;



}
