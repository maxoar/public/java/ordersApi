//do jQuery stuff when DOM is ready

$(document).ready(function() {
    //var HOST = "http://localhost";
    //var PORT = "8082";
    //var URL = HOST+":"+PORT+"/api";
    var HEROKU_HOST = "https://secret-ridge-15376.herokuapp.com/"
    var URL = HEROKU_HOST+"/api";

    $("#order-form").submit(function(event) {

        /* stop form from submitting normally */
        event.preventDefault();

        /* get some values from elements on the page: */
        let $form = $(this),
            name = $form.find('input[id="order-name"]').val(),
            price = $form.find('input[id="order-price"]').val(),
            discount = $form.find('input[id="order-discount"]').val(),
            uri = $form.attr('action');

        let order = { "name":name, "price":price };
        if( discount !== undefined && discount.length > 0){
            $.extend( order, {"discount":discount })
        }

        let onSuccess = function(data) {
            let content = $(data).find('#content');
            let notif = $("#order-create-notif");
            notif.html([
                { statusClass: 'alert-success', text: "Orden creada con éxito!"}
            ].map(NotifTemplate).join(''));
        }

        let onFailure = function(data) {
            let content = $(data).find('#content');
            let notif = $("#order-create-notif");
            notif.html([
                { statusClass: 'alert-danger', text: "Error al crear orden."}
            ].map(NotifTemplate).join(''));
        }

        $.ajax({
            type: "POST",
            url: URL+uri,
            data: JSON.stringify(order),
            success: onSuccess,
            error: onFailure,
            dataType: 'json',
            contentType: 'application/json;'
        });
    });

    const NotifTemplate = ({ statusClass, text }) => `
         <div class="alert ${statusClass}" role="alert">
            <span class="notif-text">${text}</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        `;
});